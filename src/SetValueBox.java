import java.awt.Color;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;


import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

interface ValueChangeListener {
	  public void valueChanged(Integer value);
}

public class SetValueBox extends JPanel {
	
	private List<ValueChangeListener> listeners;
	private JLabel setValue;
	private Integer currentValue;
	
	public SetValueBox(){
		listeners = new ArrayList<ValueChangeListener>();
		currentValue = 0;
		GroupLayout mainLayout = new GroupLayout(this);
		this.setLayout(mainLayout);
		//this.add(label);
		setValue = new JLabel(currentValue.toString());
		setValue.setFont(new Font("Arial",1,16));
		setValue.setOpaque(true);
		setValue.setBackground(Color.darkGray);
		setValue.setForeground(Color.white);
		setValue.setBorder(new LineBorder(Color.darkGray, 16, false));
		//this.add(setValue);
		//JButton plusButton = new JButton("+");
		//JButton minusButton = new JButton("-");
		JLabel plusLabel = new JLabel("+");
		plusLabel.setOpaque(true);
		plusLabel.setFont(new Font("Courier",0,16));
		plusLabel.setHorizontalTextPosition(SwingConstants.CENTER);
		plusLabel.setBackground(Color.darkGray);
		plusLabel.setForeground(Color.white);
		plusLabel.setBorder(new LineBorder(Color.darkGray, 2, false));
		JLabel minusLabel = new JLabel("-");
		minusLabel.setOpaque(true);
		minusLabel.setFont(new Font("Courier",0,16));
		minusLabel.setHorizontalTextPosition(SwingConstants.CENTER);
		minusLabel.setBackground(Color.darkGray);
		minusLabel.setForeground(Color.white);
		minusLabel.setBorder(new LineBorder(Color.darkGray, 2, false));
		
		
		mainLayout.setAutoCreateGaps(true);
		mainLayout.setAutoCreateContainerGaps(true);
		mainLayout.setHorizontalGroup(
				mainLayout.createSequentialGroup()
				.addGroup(mainLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
						.addComponent(setValue))
						.addGroup(mainLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
								.addComponent(minusLabel)
								.addComponent(plusLabel)));

		mainLayout.setVerticalGroup(
				mainLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGroup(mainLayout.createSequentialGroup()
						.addComponent(minusLabel).addComponent(plusLabel))
				.addGroup(mainLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
						.addComponent(setValue))
				
				);
		
		
		plusLabel.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				currentValue += 1;
				notifyListeners();
			}
		});
		
		minusLabel.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if(currentValue > 0){
					currentValue -= 1;
					notifyListeners();
				}
			}
		});
	}
	

	// Adds a listener that should be notified.
	public void addValueListener(ValueChangeListener listener) {
		listeners.add(listener);
	}
	
	// Notifies all the listeners which should be told that the percentage
	// has changed.
	private void notifyListeners() {
		setValue.setText(currentValue.toString());
		for (ValueChangeListener listener : listeners) {
			listener.valueChanged(currentValue);
		}
	}
}