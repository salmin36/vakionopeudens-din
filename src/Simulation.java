import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.Timer;
import malli.*;

public class Simulation {
	//Only used  for testing
	private static Random generator;
	//Pointer to CruiseControl object
	private CruiseControl m_cruiseControl;
	//Pointer to ControlSystem object
	private ControlSystem m_controlSystem;
	//Used to store throttle position given by ControlSystem
	private Integer m_throttleValue;
	//Used to store throttle position given by CruiseControl(users input) 
	private Integer m_throttleSetValue;
	//Used to store brake position
	private Integer m_brakeValue;
	//Used to store speed setValue set by user
	private Integer m_speedSetValue;
	//Timer that is used to do all the scheduled actions once every 100 ms 
	private Timer m_refreshTimer;
	//Variable that is used to store current simulation state
	private Types.SimulationState m_simulationState;
	//Used to store total distance traveled in km
	private Double m_totalDistance;
	//Used to store trip distance traveled in km 
	private Double m_tripDistance;
	//Used to store current speed in km/h
	private Double m_speed;
	//Used to store distance to car in front in meters
	private Double m_distance;
	//Used to determine that graph update is done 200 ms intervals when m_refreshTimer inteval is 100 ms 
	private boolean m_updateGraph; 
	//Pointer to Malli interface
	Malli m_malli;
	
	
	public Simulation(CruiseControl cruise){
		//Initialize private attributes
		m_cruiseControl = cruise;
		m_controlSystem = new ControlSystem(this);
		m_throttleValue = 0;
		m_throttleSetValue = 0;
		m_brakeValue = 0;
		m_speedSetValue = 0;
		m_simulationState = Types.SimulationState.STATE_OFF;
		m_totalDistance = 0.0;
		m_tripDistance = 0.0;
		m_speed = 0.0;
		m_distance = 0.0;
		m_updateGraph = false;
		m_malli = new Malli();
		
		//This is called once every 100ms
		m_refreshTimer = new Timer(100, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//Reverse m_update boolean 
				m_updateGraph ^= true;
				calculateSpeedAndDistance();
				
				//Check is there is a car in front
				if(m_malli.onkoKohdeSeurannassa()){
					m_distance = m_malli.annaEtaisyysKohteeseen();
				}

				//This is done once every 200ms
				if(m_updateGraph){
					m_cruiseControl.updateSpeedGraph(m_speed);
					m_cruiseControl.updateDistanceGraph(m_distance);
					m_cruiseControl.onDistanceChanged(m_distance.intValue());
					m_cruiseControl.onTotalTravelDistanceChanged(m_totalDistance);
					m_cruiseControl.onTripTravelDistanceChanged(m_tripDistance);
					m_cruiseControl.onSpeedChanged(m_speed.intValue());
				}
			}
		});
		//Set repeats so that it is triggered with set interval
		m_refreshTimer.setRepeats(true);
		m_simulationState = Types.SimulationState.STATE_OFF;
		
	}
	
	//Function that is used to start simulation 
	public void startSimulation(){
		m_refreshTimer.start();
	}
	
	//Function that is used to stop simulation
	public void stopSimulation(){
		m_refreshTimer.stop();
		m_malli.nollaaMalli();
		//Kutsutaan Simulation luokan changeControlState(false)
	}
	
	//Function that is used to save throttle value 
	public void setThrottleValue(Integer value){
		if(value < 0 && value > 100){
			throw new IllegalArgumentException();
		}
		m_throttleSetValue = value;
	}
	
	//Function that is used to save brake value
	public void setBrakeValue(Integer value){
		if(value < 0 && value > 100){
			throw new IllegalArgumentException();
		} 
		m_brakeValue = value;
	}
	
	//Function that is used to save speed set value
	public void setSpeedSetValue(Integer value){
		if(value < 0 && value > 100){
			throw new IllegalArgumentException();
		}
		m_speedSetValue = value;
	}
	
	//Function that is used to get Simulation state
	public Types.SimulationState getSimulationState(){
		return m_simulationState;
	}
	
	
	
	
	//Function that is used to set Simulation state
	public void setSimulationState(Types.SimulationState state){
		m_simulationState = state;
	}
	
	//Function that is used to ask if controlSystem is on or off
	public boolean getControlState(){
		return m_controlSystem.getControlState();
	}
	
	public boolean changeControlState(boolean state){
		if(state){
			//Tarkista että säädön päällekytkemisen esiehdot toteutuvat
			m_controlSystem.setControlState(true);
			return true;
		}
		else{
			if(m_controlSystem.getControlState()){
				m_controlSystem.setControlState(false);
				return true;
			}
		}
		return false;
	}
	
	
	public void clearTripTravelDistance(){
		m_tripDistance = 0.0;
	}
	
	//Function calculates and saves speed and distance with information given by MALLI
	private void calculateSpeedAndDistance(){
		//Get control time interval
		Double dt = m_malli.h;
		//Get tire diameter
		Double dia = m_malli.d;
		//Brake is applied
		if(m_brakeValue != 0){
			System.out.println("m_brakeValue is non zero");
			//Give negative value for braking
			m_malli.laskeNopeus(m_brakeValue * (-1));
		}
		else{
			System.out.println("m_throttleValue == " + m_throttleValue.toString());
			//If Control is not on then accelerator pedal value is given to Malli 
			if(!m_controlSystem.getControlState()){

				m_malli.laskeNopeus(m_speedSetValue);
			}
			//Otherwise value calculated by ControlSystem is given to MAlli
			else{
				m_malli.laskeNopeus(m_throttleValue);
			}
		}
		
		//Traveled distance in meters
		Double travelledDistance = Math.PI * dia * m_malli.annaKierrokset();
		//Add traveled distance to trip and total distance
		m_totalDistance += travelledDistance / 1000; 
		m_tripDistance += travelledDistance / 1000;
		//Instantaneous speed in km/h
		m_speed = (travelledDistance / dt) *3.6;
		System.out.println("CalculateSpeedAndDistance: m_speed == " + m_speed.toString());
	}
	
	
	
}
