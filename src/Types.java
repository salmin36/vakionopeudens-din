
public class Types {
	public enum SimulationState{
		STATE_OFF, STATE_ON, 
		STATE_WARNING, STATE_EMERGENCY
	}

	public enum ControlMode {
		MODE_CRUISE("Nopeus säätyy asetusarvon mukaan"), 
		MODE_TRAFFIC("Nopeus säätyy et�isyyden mukaan");
		
		private String m_text;
		
		private ControlMode(String m_text){
			this.setText(m_text);
		}

		public String getText() {
			return m_text;
		}

		public void setText(String text) {
			this.m_text = text;
		}
	}
	
	public enum ControlAlarm{
		ALARM_OFF, ALARM_WARNING, ALARM_EMERGENCY
	}
}
