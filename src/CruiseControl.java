import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.channels.SeekableByteChannel;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.border.LineBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;



public class CruiseControl {
	public static void main(String[] args) {
		CruiseControl cruiseControl = new CruiseControl();
	}

	
	//Slider that is used to accelerate the car
	private final JSlider acceleratorSlider;
	//Slider that is used to brake the car
	private final JSlider brakeSlider;
	//Label that is used to display traveled trip distance
	private JLabel tripDistanceValueLabel;
	private JButton tripDistanceClear;
	//Label that is used to display total distance traveled
	private JLabel totalDistanceValueLabel;
	//Label that is used to display current contolMode
	private JLabel statusLabel;
	//Label that is used to display brakePedalValue and its background color is used to 
	//warn user
	private JLabel brakePedalValueLabel;
	//Label that is used to display current speed
	private JLabel curSpeedValueLabel;
	//Label that is used to display current distance to car in front
	private JLabel distanceValueLabel;
	//Button that is used to toggle Simulation on/off
	private JButton simulationButton;
	//Button that is used to toggle Control on/off
	private JButton controlSystemButton;
	//Graph for speed 
	private final Graph speedGraph;
	//Graph for distance
	private final Graph distanceGraph;
	//Component that has set value and its increment and decrement buttons
	private SetValueBox setValueBox;
	//Pointer to simulation
	private Simulation m_simulation;
	
	
	public CruiseControl(){
		//Create new Simulation and give pointer to it
		m_simulation = new Simulation(this);
		
		JFrame mainFrame = new JFrame("Vakionopeudensäädin");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		GridBagLayout grid = new GridBagLayout();
	    GridBagConstraints c = new GridBagConstraints();
				
		//mainFrame.setLayout(new FlowLayout());
		mainFrame.setLayout(grid);
		mainFrame.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		
		JPanel graphFrame = new JPanel();
		JPanel labelFrame = new JPanel();
		//graphFrame.setLayout(new FlowLayout());
		graphFrame.setLayout(grid);
		//graphFrame.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		labelFrame.setLayout(grid);
		//labelFrame.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		//Speed Label
		JLabel speedLabel = new JLabel("Ajonopeus:");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.WEST;
		graphFrame.add(speedLabel,c);
		
		//Distance Label
		JLabel distanceLabel = new JLabel("Etäisyys:");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 0;
		c.insets = new Insets(0,50,0,0);
		c.anchor = GridBagConstraints.WEST;
		graphFrame.add(distanceLabel,c);
				
		//Speed Graph
		speedGraph = new Graph(0, 200);
		speedGraph.addGroup(Color.green);
		speedGraph.addGroup(Color.red);
		c.fill = GridBagConstraints.VERTICAL;
		c.gridx = GridBagConstraints.RELATIVE;
		c.gridy = 1;
		c.insets = new Insets(0,0,0,0);
		c.anchor = GridBagConstraints.WEST;
		graphFrame.add(speedGraph,c);
		
		//Distance graph
		distanceGraph = new Graph(0, 250);
		distanceGraph.addGroup(Color.green);
		c.fill = GridBagConstraints.VERTICAL;
		c.gridx = GridBagConstraints.RELATIVE;
		c.gridy = 1;
		c.insets = new Insets(0,50,0,0);
		c.anchor = GridBagConstraints.WEST;
		graphFrame.add(distanceGraph,c);	

		//Current Speed Label
		JLabel curSpeedLabel = new JLabel("Hetkellinen nopeus:");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(0,0,0,0);
		c.anchor = GridBagConstraints.WEST;
		labelFrame.add(curSpeedLabel,c);
		
		//Set Value Label
		JLabel setValueLabel = new JLabel("Asetusarvo:");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = GridBagConstraints.RELATIVE;
		c.gridy = 0;
		c.insets = new Insets(0,25,0,0);
		labelFrame.add(setValueLabel,c);

		//Distance Label 2
		JLabel distance2Label = new JLabel("Etäisyys:");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = GridBagConstraints.RELATIVE;
		c.gridy = 0;
		c.insets = new Insets(0,100,0,0);
		labelFrame.add(distance2Label,c);
		
		LineBorder lb = new LineBorder(Color.darkGray,16,false);
		
		//Current Speed Value Label
		curSpeedValueLabel = new JLabel("0 km/h");
		curSpeedValueLabel.setFont(new Font("Arial",1,16));
		curSpeedValueLabel.setOpaque(true);
		curSpeedValueLabel.setBorder(lb);
		curSpeedValueLabel.setBackground(Color.darkGray);
		curSpeedValueLabel.setForeground(Color.white);
		curSpeedValueLabel.setHorizontalAlignment(SwingConstants.CENTER);
		c.fill = GridBagConstraints.CENTER;
		c.gridx = 0;
		c.gridy = 1;
		c.insets = new Insets(0,45,0,0);
		c.anchor = GridBagConstraints.WEST;
		labelFrame.add(curSpeedValueLabel,c);

		//Set Value Box
		setValueBox = new SetValueBox();
		c.fill = GridBagConstraints.CENTER;
		c.gridx = 1;
		c.gridy = 1;
		c.insets = new Insets(0,0,0,0);
		c.anchor = GridBagConstraints.WEST;
		labelFrame.add(setValueBox,c);

		//Distance Value Label
		distanceValueLabel = new JLabel("0 m");
		distanceValueLabel.setFont(new Font("Arial",1,16));
		distanceValueLabel.setOpaque(true);
		distanceValueLabel.setBorder(lb);
		distanceValueLabel.setBackground(Color.darkGray);
		distanceValueLabel.setForeground(Color.white);
		distanceValueLabel.setHorizontalAlignment(SwingConstants.CENTER);
		c.fill = GridBagConstraints.CENTER;
		c.gridx = 2;
		c.gridy = 1;
		c.insets = new Insets(0,110,0,0);
		c.anchor = GridBagConstraints.WEST;
		labelFrame.add(distanceValueLabel,c);

		//Graafi-frame
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = GridBagConstraints.RELATIVE;
		c.insets = new Insets(0,0,0,0);
		c.anchor = GridBagConstraints.WEST;
		mainFrame.add(graphFrame,c);
		
		//Label-frame
		c.fill = GridBagConstraints.VERTICAL;
		c.gridx = 0;
		c.gridy = GridBagConstraints.RELATIVE;
		mainFrame.add(labelFrame,c);
		
		//Accelerator-frame
		JPanel acceleratorFrame = new JPanel();
		acceleratorFrame.setLayout(new GridBagLayout());
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.CENTER;
		acceleratorFrame.add(new JLabel("Kaasu:"),c);
		
		//Accelerator slider 
		acceleratorSlider = new JSlider(JSlider.VERTICAL, 0, 100, 0);
		acceleratorSlider.setMinimumSize(new Dimension(20, 100));
		
		//Add slider to acceleratorFrame
		c.gridy = 1;
		acceleratorFrame.add(acceleratorSlider,c);
		
		//Accelerator pedal value
		final JLabel accPedalValueLabel = new JLabel("0");
		accPedalValueLabel.setFont(new Font("Arial",1,16));
		accPedalValueLabel.setOpaque(true);
		accPedalValueLabel.setBackground(Color.darkGray);
		accPedalValueLabel.setForeground(Color.white);
		accPedalValueLabel.setMinimumSize(new Dimension(23,20));
		c.gridy = 2;
		acceleratorFrame.add(accPedalValueLabel,c);
		
		//Add change listener to accelerator slider
		acceleratorSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				accPedalValueLabel.setText(String.valueOf(acceleratorSlider.getValue()));
			}
		});
		
		
		//Add acceleratorFrame to mainFrame
		c.anchor = GridBagConstraints.LINE_START;
		c.gridy = 3;
		c.gridx = 0;
		mainFrame.add(acceleratorFrame,c);
		
		
		//Brake-frame
		JPanel brakeFrame = new JPanel();
		brakeFrame.setLayout(new GridBagLayout());
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.CENTER;
		brakeFrame.add(new JLabel("Jarru:"),c);
		
		//Brake slider 
		brakeSlider = new JSlider(JSlider.VERTICAL, 0, 100, 0);
		brakeSlider.setMinimumSize(new Dimension(20, 100));
		c.gridy = 1;
		brakeFrame.add(brakeSlider,c);
		
		//Brake pedal value
		brakePedalValueLabel = new JLabel("0");
		brakePedalValueLabel.setFont(new Font("Arial",1,16));
		brakePedalValueLabel.setOpaque(true);
		brakePedalValueLabel.setBackground(Color.green);
		brakePedalValueLabel.setForeground(Color.white);
		brakePedalValueLabel.setMinimumSize(new Dimension(23,20));
		c.gridy = 2;
		brakeFrame.add(brakePedalValueLabel,c);
		
		
		//Add change listener to accelerator slider
		brakeSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				brakePedalValueLabel.setText(String.valueOf(brakeSlider.getValue()));
			}
		});


		//Add brakeFrame to mainFrame
		c.anchor = GridBagConstraints.LINE_START;
		c.gridy = 3;
		c.insets = new Insets(0,100,0,0);
		c.gridx = 0;
		//c.anchor = GridBagConstraints.ABOVE_BASELINE_LEADING;
		mainFrame.add(brakeFrame,c);
		
		
		//Status panel
		JPanel statusPanel = new JPanel();
		statusPanel.setLayout(new GridBagLayout());
		
		//Label that has information about control mode
		statusLabel = new JLabel("Nopeus säätyy asetusarvon mukaan");
		c.insets = new Insets(20,0,0,0);
		c.gridy = 0;
		c.gridx = 0;
		c.weightx = 2;
		statusPanel.add(statusLabel,c);
		
		//Label for total distance traveled
		c.gridx = 0;
		c.gridy = 1;
		statusPanel.add(new JLabel("Kokonaismatka:"),c);
		
		//Label for trip distance traveled
		c.gridx = 1;
		c.gridy = 1;
		statusPanel.add(new JLabel("Trippimatka:"),c);
		
		//Box for total distance traveled
		totalDistanceValueLabel = new JLabel("0 km");
		totalDistanceValueLabel.setFont(new Font("Arial",1,16));
		totalDistanceValueLabel.setOpaque(true);
		totalDistanceValueLabel.setBackground(Color.darkGray);
		totalDistanceValueLabel.setForeground(Color.white);
		totalDistanceValueLabel.setMinimumSize(new Dimension(100,20));
		c.gridx = 0;
		c.gridy = 2;
		statusPanel.add(totalDistanceValueLabel,c);

		//Box for trip distance traveled
		tripDistanceValueLabel = new JLabel("0 km");
		tripDistanceValueLabel.setFont(new Font("Arial",1,16));
		tripDistanceValueLabel.setOpaque(true);
		tripDistanceValueLabel.setBackground(Color.darkGray);
		tripDistanceValueLabel.setForeground(Color.white);
		tripDistanceValueLabel.setMinimumSize(new Dimension(100,20));
		c.gridx = 1;
		c.gridy = 2;
		statusPanel.add(tripDistanceValueLabel,c);
		
		//Button to clear trip distance
		tripDistanceClear = new JButton("Clear");
		c.gridx = 2;
		c.gridy = 2;
		statusPanel.add(tripDistanceClear,c);
		
		

		//Add statusPanel to mainFrame
		c.anchor = GridBagConstraints.LINE_START;
		c.gridy = 3;
		c.insets = new Insets(0,150,0,0);
		c.gridx = 0;
		mainFrame.add(statusPanel, c);
		
		
		//Panel that has simulation and control system on/off buttons
		JPanel togglePanel = new JPanel();
		togglePanel.setLayout(new GridBagLayout());
		c.gridx = 0;
		c.gridy = 0;
		//c.anchor = GridBagConstraints.CENTER;
		togglePanel.add(new JLabel("Simulaatio"),c);
		c.gridx = 1;
		c.gridy = 0;
		togglePanel.add(new JLabel("Säätöjärjestelmä"),c);
		simulationButton = new JButton("Start");
		simulationButton.setForeground(Color.green);
		c.gridx = 0;
		c.gridy = 1;
		togglePanel.add(simulationButton, c);
		controlSystemButton = new JButton("Start");
		controlSystemButton.setForeground(Color.green);
		c.gridx = 1;
		c.gridy = 1;
		togglePanel.add(controlSystemButton,c);
		c.anchor = GridBagConstraints.LINE_END;
		c.gridy = 4;
		c.insets = new Insets(0,200,0,0);
		c.gridx = 0;
		mainFrame.add(togglePanel, c);
		
		
		
		//Resize to fit all child components
		mainFrame.pack();
		mainFrame.setVisible(true);
		
		
		/*Timer timer = new Timer(200,new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int myInt = Math.abs(generator.nextInt() %100);
				
				speedGraph.addValue(0,myInt);
				myInt = Math.abs(generator.nextInt() %100);
				speedGraph.addValue(1,myInt);
				
				speedGraph.updateUI();
				myInt = Math.abs(generator.nextInt() % 250);
				distanceGraph.addValue(0,myInt);
				distanceGraph.updateUI();
			}
		});
		timer.setRepeats(true);
		//timer.start();
		*/
		//onBreakeSetValueChanged
		brakeSlider.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				//When brake is applied accelerator pedal is set to zero
				acceleratorSlider.setValue(0);
				int value = brakeSlider.getValue();
				if(value >= 0 && value <= 100){
					m_simulation.setBrakeValue(value);
				}
				else{
					throw new IllegalArgumentException();
				}
			}
		});
		
		//onThrottleSetValueChanged
		acceleratorSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				//When accelerator pedal is applied brake pedal is set to zero
				brakeSlider.setValue(0);
				int value = acceleratorSlider.getValue();
				System.out.println("Accelerator slider value changed to " + value);
				if(value >= 0 && value <= 100){
					m_simulation.setSpeedSetValue(value);
				}
				else{
					throw new IllegalArgumentException();
				}
			}
		});	
		
		
		//onSpeedSetValueChanged
		setValueBox.addValueListener(new ValueChangeListener() {
			@Override
			public void valueChanged(Integer value) {
				//Tässä kutsutaan Simulation luokan setSpeedSetValue value parametrilla
				System.out.println("ValueChanged käsittelijässä: " + value.toString());
			}
		});
		
		
		//Function that is used to change simulation visual state
		simulationButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Types.SimulationState state = m_simulation.getSimulationState();
				if(state == Types.SimulationState.STATE_OFF){
					m_simulation.setSimulationState(Types.SimulationState.STATE_ON);
					m_simulation.startSimulation();
					simulationButton.setText("STOP");
					simulationButton.setForeground(Color.red);
				}
				else{
					m_simulation.setSimulationState(Types.SimulationState.STATE_OFF);
					m_simulation.stopSimulation();
					simulationButton.setText("START");
					simulationButton.setForeground(Color.green);
				}
			}
		});
		
		//onControlStateChanged
		controlSystemButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(m_simulation.getControlState()){
					if(m_simulation.changeControlState(false)){
						controlSystemButton.setText("Start");
						controlSystemButton.setForeground(Color.green);
					}
					
				}
				else{
					if(m_simulation.changeControlState(true)){
						controlSystemButton.setText("Stop");
						controlSystemButton.setForeground(Color.red);
					}
				}
			}
		});
		
		
		//onClearTripTravel
		tripDistanceClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tripDistanceValueLabel.setText("0 km");
				m_simulation.clearTripTravelDistance();
			}
		});
		
	}
	
	//Function that is used by Simulation object to tell when throttle value has been changed
	public void onThrottleValueChanged(Integer value){
		if(value >= 0 && value <= 100){
			acceleratorSlider.setValue(value);
		}
		else{
			throw new IllegalArgumentException();
		}
	}
	
	//Function that is used by Simulation object to tell when brake value has been changed 
	public void onBrakeValueChanged(Integer value){
		if(value >= 0 && value <= 100){
			brakeSlider.setValue(value);
		}
		else{
			throw new IllegalArgumentException();
		}
	}
	
	//Function that is used to update trip distance
	public void onTripTravelDistanceChanged(double value){
		tripDistanceValueLabel.setText(doubleToStringWithPrecision(value) + " km");
	}
	
	//Function that is used to update total distance 
	public void onTotalTravelDistanceChanged(double distance){
		totalDistanceValueLabel.setText(doubleToStringWithPrecision(distance) + " km");
	}
	
	//Function is used to change control system visual state
	public void onControlStateChanged(){
		//Kutsu Simulation luokan getControlState funktota selvitääkseen mihinkä tilaan pitää muuttua
	}
	
	//Function is used toChange ControlMode text
	public void onControlModeChanged(Types.ControlMode mode){
		statusLabel.setText(mode.getText());
	}
	
	//Function is used to change color of warning light according to state
	public void onEmergencyStateChanged(Types.ControlAlarm state){
		switch(state){
			case ALARM_EMERGENCY:
				brakePedalValueLabel.setBackground(Color.red);
				break;
			case ALARM_WARNING:
				brakePedalValueLabel.setBackground(Color.yellow);
				break;
			case ALARM_OFF:
				brakePedalValueLabel.setBackground(Color.green);
				break;
			default:
				break;
		}
	}
	
	//Function that is used to update current speed
	public void onSpeedChanged(Integer speed){
		curSpeedValueLabel.setText(Integer.toString(speed) + " km/h");
	}
	
	//Function is used to update current distance to car in front
	public void onDistanceChanged(Integer distance){
		distanceValueLabel.setText(distance.toString() + " m");
	}
	
	//Function is used to make visual changes to user interface when control is stopped
	public void onControlEnd(){
		controlSystemButton.setText("Start");
		controlSystemButton.setForeground(Color.green);
	}
	
	//Function that is used to update speed Graph
	public void updateSpeedGraph(Double value){
		speedGraph.addValue(0,value.intValue());
		speedGraph.updateUI();
	}
	
	//Function that is used to update distance Graph
	public void updateDistanceGraph(Double value){
		distanceGraph.addValue(0,value.intValue());
		distanceGraph.updateUI();
	}
	
	//Function takes Double and return its value in type String with precision of one
	public String doubleToStringWithPrecision(Double value){
		String str = value.toString();
		int index = str.indexOf('.');
		return str.substring(0, index+2);
	}
}