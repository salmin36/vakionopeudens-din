import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

public class Graph extends JPanel {
	private final int M_MAX_VALUE_COUNT = 50;
	private int m_low, m_high, m_dx, m_yx;
	private ArrayList<Integer[]> m_valueList;
	private ArrayList<Color> m_colorsList;
	
	public Graph(Integer low, Integer high){
		m_low = low;
		m_high = high;
		m_valueList = new ArrayList<Integer[]>();
		m_colorsList = new ArrayList<Color>();
		setMinimumSize(new Dimension(200, 200));
	}
	
	public void addGroup(Color col)
	{
		Integer[] array = new Integer[M_MAX_VALUE_COUNT];
		//Set array full of value 0
		for(int i = 0; i < M_MAX_VALUE_COUNT; ++i) {
			array[i] = 0;
		}
		m_valueList.add(array);
		m_colorsList.add(col);
	}
	
	public void addGroup(Integer r, Integer g, Integer b)
	{
		Color col = new Color(Math.abs(r),Math.abs(g),Math.abs(b));
		Integer[] array = new Integer[M_MAX_VALUE_COUNT];
		//Set array full of value 0
		for(int i = 0; i < M_MAX_VALUE_COUNT; ++i) {
			array[i] = 0;
		}
		m_valueList.add(array);
		m_colorsList.add(col);
	}
	
	public void addValue(Integer graphIndex, Integer value){
		System.out.println("added value("+graphIndex+"): " + value.toString());
		moveToNextPositon(graphIndex.intValue());
		Integer[] ints = m_valueList.get(graphIndex);
		value = mapToLocal(value);
		ints[M_MAX_VALUE_COUNT-1] = value;
		m_valueList.set(graphIndex, ints);		
	}
	
	public void addValue(Integer graphIndex, Double value){
		System.out.println("added value: " + value.toString());
		moveToNextPositon(graphIndex.intValue());
		Integer[] ints = m_valueList.get(graphIndex);
		Integer valueAsInt = value.intValue();
		valueAsInt = mapToLocal(valueAsInt);
		ints[M_MAX_VALUE_COUNT-1] = valueAsInt;
		m_valueList.set(graphIndex, ints);
	}
	
	public Dimension getPreferredSize(){
		return new Dimension(250, 200);
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		setBackground(Color.black);
		m_dx = getWidth()/M_MAX_VALUE_COUNT;
		
		for(int j = 0; j < m_valueList.size(); j++)
		{
			g.setColor(m_colorsList.get(j));
			for(int i = 0; i < M_MAX_VALUE_COUNT-1; ++i){
				g.drawLine(i * m_dx, m_valueList.get(j)[i], (i+1) * m_dx , m_valueList.get(j)[i+1]);
			}
		}
		
		g.setColor(Color.white);
		g.drawString(Integer.toString(m_low), 10, getHeight()- 10);
		g.drawString(Integer.toString(m_high), 10, 10);
	}
	
	//This function moves points to left by one
	private void moveToNextPositon(int j){
		Integer[] array =  m_valueList.get(j);
		
		for(int i = 0; i < M_MAX_VALUE_COUNT-1; ++i){
			array[i] = array[i+1];
		}
		m_valueList.set(j, array);
	}
	
	//Returns point in local coordinate system
	private int mapToLocal(int x){
		double result;
		double height = m_high - m_low;
		double scalingFactor = getHeight()/height;
		result = x*scalingFactor;
		result = Math.abs(getHeight() - result);
		return (int)result;
	}
}