
public class ControlSystem {

	//Pointer to Simulation object that owns this object
	private Simulation m_simulation;
	//Value that is used to determine if controlSystem is on or off
	private boolean m_controlState;
	private Types.ControlMode m_controlMode;
	public ControlSystem(Simulation simulation){
		m_simulation = simulation;
		m_controlMode = Types.ControlMode.MODE_CRUISE;
		setControlState(false);
	}
	
	public Types.ControlMode getControlMode(){
		return m_controlMode;
	}
	

	public boolean getControlState() {
		return m_controlState;
	}

	public void setControlState(boolean controlState) {
		m_controlState = controlState;
	}
}
